\begin{tabular}[b]{| l | r r | r r | r r |} \hline 
Vec size & \multicolumn{2}{c |}{$10^3$ - $10^5$} & \multicolumn{2}{c |}{$10^5$ - $10^7$} & \multicolumn{2}{c |}{$10^7$ - $10^8$} \\ \hline 
Operation & latency & bandwidth & latency & bandwidth & latency & bandwidth \\ \hline 
VecDot & 16 & 3,900 & - & 2,500 & - & 2,400 \\ 
VecAXPY & 6 & 5,600 & - & 3,500 & - & 3,400 \\ 
VecSet & - & 8,000 & - & 2,800 & - & 2,400 \\ 
VecCopy & - & 7,200 & - & 3,300 & - & 3,200 \\ 
\hline \end{tabular}