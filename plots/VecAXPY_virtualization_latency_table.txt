\begin{tabular}[b]{| c | c c |} \hline 
MPI ranks & latency & bandwidth \\ \hline 
1 & 31 & 99,000 \\ 
2 & 33 & 99,000 \\ 
3 & 34 & 96,000 \\ 
4 & 38 & 96,000 \\ 
5 & 40 & 96,000 \\ 
6 & 43 & 99,000 \\ 
7 & 45 & 105,000 \\ 
8 & 46 & 105,000 \\ 
\hline \end{tabular}