%
%  computes the values in Discussion section

lg = 89e-6; lc = 6e-6 ; bg= 5.176e12;  bc = 256e9;

display(" size required for 90 percent of GPU performance")
n = 9*lg*bg

display("ratio of times at 90% of GPU for GPU and CPU")

tctg = (lc + n/bc)/(lg + n/bg)

display(" nc per CPU needed to achieve same time on CPU as GPU")

 nc = bc*(n/bg + lg - lc)

display("number of nodes needed achieve same time on CPU as GPU")

 n/nc

display("CPU nodes to achieve beta latency of CPU (not GPU)")

(lg*bg)/(lc*bc)


display(" number of nodes to speedup by k = 2")
k = 2 ; beta = 9;

(k*beta)/(beta + 1 - k)

display(" number of nodes to speedup by k = 2")
        

k = 8


(k*beta)/(beta + 1 - k)


