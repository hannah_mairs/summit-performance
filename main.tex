\documentclass{article}
\usepackage{graphicx}
\usepackage{float}
\usepackage{pdfpages}

\usepackage[a4paper, total={6.5in, 9.25in}]{geometry}

\title{Evaluation of PETSc on a Heterogeneous Architecture \\ the OLCF Summit System \\ Part I: Vector Node Performance}
\author{Hannah Morgan, Richard Tran Mills, Barry Smith\\Mathematics and Computer Science Division \\ Argonne National Laboratory}
\date{}

\begin{document}

\includepdf[pages={1,2}]{ANL_tech_report_title_page.pdf}
\includepdf[pages={4}]{ANL_tech_report_title_page.pdf}
\includepdf[pages={3}]{ANL_tech_report_title_page.pdf}



\maketitle

\begin{abstract}
Our goal in this report is to understand the basic performance of PETSc vector operations on a single node of the Oak Ridge Leadership Computing Facility system Summit. We describe the Summit system and present data collected from a variety of vector operations. Limited analysis is also presented.
% We summarize our findings and point out pitfalls and future directions.
\end{abstract}

\section{Introduction}\label{sec:introduction}

We report on  the performance of the Portable, Extensible Toolkit for
Scientific Computation (PETSc) \cite{petsc-user-ref, petsc-web-page} vector operations on a single node of the IBM/NVIDIA Summit computing system \cite{summit-web-page} at the Oak Ridge Leadership Computing Facility (OLCF). PETSc provides NVIDIA GPU support for vector and sparse matrices based on CUDA and the cuBLAS and cuSPARSE libraries \cite{minden2013preliminary}. Using the organization of the PETSc library, many PETSc solvers and preconditioners are able to run with GPU vector and matrix implementations. This report summarizes the basic design of a Summit node, the performance of PETSc vector operations, and provides a limited analysis of the results. The planned exascale computing systems have a similar design to Summit. Thus, it is important to have a well-developed understanding of Summit in preparation for these systems. This document is not intended to provide a strict benchmarking of the Summit system, rather it is to develop an understanding of systems similar to Summit to guide PETSc development.
 

\section{The Summit System and Experimental Setup} \label{sec:summitsystem}

Each node on Summit is equipped with six NVIDIA Volta V100 GPU accelerators and two IBM POWER9 processors, each with 21 cores available to users, for a total of 42 cores (Figure \ref{fig:summit_node}, left). A GPU on Summit can be utilized by more than one MPI rank simultaneously, the term NVIDIA uses is Multi-Process Service (MPS), we use the term virtualization. For example a single physical GPU may be treated as 4 virtual GPUs by four MPI ranks running on four CPU cores. Virtualization on Summit is enabled by submitting jobs with the bsub option \texttt{-alloc\_flags gpumps}. 

To obtain high performance on a Summit node for benchmarking (that is by varying the number of CPU cores and GPUs used) it is important to select the optimal physical cores and associate them with appropriate GPUs. The two POWER9 processors are each connected directly to three of the GPUs, so cores associated with each process should, for best performance, utilize one of the three GPUs connected to the same socket. By default when only utilizing the CPUs (for example, to compare GPU and CPU performance), the Summit CPU cores are assigned from the first socket until it is full before using cores from the second socket; benchmarking numbers that utilize these defaults are not appropriate.  To utilize the CPUs effectively use two resource sets each with 21 associated cores to take advantage of the combined memory bandwidth of both sockets of the Summit node (red and yellow in Figure \ref{fig:summit_node}, right) is needed. We accomplish this with the jsrun options \texttt{--nrs 2 --cpu\_per\_rs 21}. Furthermore, adjacent Summit CPU cores share L2 and L3 cache so we use the jsrun option \texttt{--bind packed:2} to bind each MPI rank to two CPU cores so that each MPI rank has dedicated L2 and L3 cache. We also launch all jobs using the \texttt{--launch\_distribution cyclic} option so that MPI ranks are assigned to resource sets in a circular fashion. These choices are shown with seven MPI ranks in Figure \ref{fig:summit_node}, right. 

The Summit CPU operating system uses traditional Unix memory management with memory pages. In order to obtain high performance in copies between the CPU and the GPU the CPU memory must be ``pinned''. This is done by allocating the memory with a special routine \texttt{cudaMallocHost} instead of the system \texttt{malloc}. Our benchmarks are computed with pinned CPU memory. 

Collecting useful and appropriate timings on a GPU based system is not always straightforward. It is possible to collect timings on the Nvidia GPUs using \texttt{cudaEventRecord} or on the CPU cores using traditional Unix timers. Which timers to use depends on the goals of the analysis. In addition, care must be taken in timing asynchronous calls to the GPU. These can be subtle, for example, small (less than 64 kilobytes) cudaMemcpy() calls to the GPU are asynchronous so it is easy to get misleading timings from the CPU on these transfers without care. In this report, since it treats PETSc in the traditional host-device model where the outer code is MPI parallel and we are interesting in the higher level details of the performance, we always use the MPI rank timers (using PETSc logging). This may result in over-timing some operations since it includes the time to synchronize back to the CPU when in actual optimized runs that synchronization may not take place. 

For these results PETSc 3.12 was with built with CUDA version 10.1.168 and PGI compilers version 19.4. The codes used for the measurements in this report are available in the PETSc repository. They are in src/vec/vec/examples/tutorials/performance.c, this file also indicates how you can obtain the scripts for producing the graphs. Peak performance numbers for Summit are from \cite{summit-performance}. When comparing GPU to CPU performance we use all 42 available cores of the CPU, this may not be the optimal value for obtaining CPU performance since the memory bandwith is shared between cores. Since our goal is not to benchmark Summit's CPU scalability, we have not collected statistics with a range of CPU cores.

\begin{figure}[t]
    \centering
    \includegraphics[width=0.45\textwidth]{plots/Summit_Node.jpg} \quad \quad
    \includegraphics[width=0.45\textwidth]{plots/jsrunVisualizer_CPU.png}
    \caption{Diagram of one node of Summit (left) and CPU allocation of 7 MPI ranks (right) from https://www.olcf.ornl.gov and https://jsrunvisualizer.olcf.ornl.gov/.}
    \label{fig:summit_node}
\end{figure}


\section{PETSc Vector Operations}\label{sec:vectoroperations}

We present and analyze results from runs of PETSc vector operations on one node of Summit.  The program creates vectors with random entries, copies them to the appropriate memory, and then performs vector operations. We report the flop rate in Mflops/s for various vector sizes utilizing either GPUs or CPUs. We also report memory throughput (in 8 Mbytes/s for easy comparison with the floating pointing results). These computations are essentially the same as the STREAMS benchmark, \cite{mccalpin1995memory}, but measured on the PETSc vector operations instead of raw C or CUDA code. We use the term ``throughput" to mean the amount of data moved divided by the time to move it. This is a combination of latency and bandwidth; introduced in Section \ref{sec:discussion}. In all cases the vectors are already in the memory of the device where the performance is being measured. All asynchronous operations are synchronized with \texttt{CudaDeviceSynchronize()} so the true time of the operation as realized on the CPU is used. The caches are flushed by performing vector operations on other vectors to remove any effects of the data, by happenstance, being in the cache. For parallel benchmarks, an MPI\_Barrier() is used immediately before the operation so that all MPI ranks start together. 

The specific vector operations are 
\begin{itemize}
    \item $ x = ax + y$, known as AXPY and implemented with PETSc's VecAXPY operation (which utilizes BLAS on the CPU and cuBLAS on the GPUs). 
    \item $ d = x^T y $, known as the dot product and implemented with PETSc's VecDot operation (this also utilizes BLAS and cuBLAS).
    \item memory copy, implemented with VecCopy (which utilizes memcpy on the CPU and cudaMemcopy on the GPUs). In addition we use cudaMemcpy for copies between the CPU and GPU. 
    \item setting all entries in a vector to zero, implemented with PETSc's VecSet function (which utilizes memset on the CPU and cudaMemset on the GPUs).
\end{itemize}
A single vector operation is timed for each operation. All vector sizes refer to the global size of the vector which may be spread among multiple computational units. There are two floating point operations per entry for the VecAXPY and VecDot operations. There are three memory accesses per vector entry for VecAXPY, two for VecDot, two for VecCopy, and one for VecSet. Thus computation of flop rates and memory throughput involves appropriate scaling by these values.

\section{Experimental results}\label{sec:experimentalresults}

Figure \ref{fig:vec_CPU_vs_GPU} presents a high level view of the performance of the Summit nodes. Details are provided below. Note the use of the log scale and that toward the right, the GPU is performing significantly better than the 42 CPU cores, while towards the left the CPU is faster. Figure \ref{fig:jed_vec_CPU_vs_GPU} presents an alternative view of the same data, known as a work-time spectrum plot \cite{fuhrer2018near}. It has the advantage that both the asymptotic bandwidth and the latency of the operations can be directly read off the figure. 

\begin{figure}[H]
    \centering
    \hspace{-.25in}
    \includegraphics[width=0.51\textwidth]{plots/VecDot_CPU_vs_GPU.png}
    \includegraphics[width=0.51\textwidth]{plots/VecAXPY_CPU_vs_GPU.png}
    \caption{Effect of vector size on vector performance and memory throughput (one MPI rank per GPU). Note the log scale.}
    \label{fig:vec_CPU_vs_GPU}
\end{figure}

\iffalse
\begin{figure}[H]
    \centering
    \hspace{-.25in}
    \includegraphics[width=0.70\textwidth]{plots/VecDot_CPU_vs_GPU.png}
    \caption{Effect of vector size on vector dot product performance and memory throughput (one MPI rank per GPU). Note the log scale.}
    \label{fig:vec_CPU_vs_GPU_vecdot}
\end{figure}

\begin{figure}[H]
    \centering
    \hspace{-.25in}
    \includegraphics[width=0.51\textwidth]{plots/VecAXPY_CPU_vs_GPU.png}
    \caption{Effect of vector size on vector AXPY performance and memory throughput (one MPI rank per GPU).}
    \label{fig:vec_CPU_vs_GPU_vecaxpy}
\end{figure}
\fi

\begin{figure}[H]
    \centering
    \hspace{-.25in}
    \includegraphics[width=0.51\textwidth]{plots/jed_VecDot_CPU_vs_GPU.png}
    \includegraphics[width=0.51\textwidth]{plots/jed_VecAXPY_CPU_vs_GPU.png}
    \caption{Effect of execution time on vector performance and memory throughput (one MPI rank per GPU). Note the log scale.}
    \label{fig:jed_vec_CPU_vs_GPU}
\end{figure}

Figures \ref{fig:VecDot_flop_rate} and \ref{fig:VecAXPY_flop_rate} compare the performance of the GPUs and CPUs for increasing vector sizes. For shorter vectors, fewer than around $ 10^6 $ entries, the CPU performance for vector operations, including copies, is far superior to the GPU. For larger sizes, $10^9$ vector entries, on the other hand, the GPU bandwidth is much higher. Performance on the CPU is relatively independent of vector size (outside the cache effect size), but for the GPUs is low except for large vectors. The drop in performance of the CPU at 22 cores is because the remaining cores share L2 and L3 caches with previously utilized cores and  share the available memory bandwidth.

\begin{figure}[H]
    \centering
    \hspace{-.25in}
    \includegraphics[width=0.51\textwidth]{plots/GPU_VecDot_flops.png}
    \includegraphics[width=0.51\textwidth]{plots/CPU_VecDot_flops.png}
    \caption{VecDot flop rate on GPUs, with one MPI rank per GPU, (left) and CPUs (right).}
    \label{fig:VecDot_flop_rate}
\end{figure}

\begin{figure}[H]
    \centering
    \hspace{-.25in}
    \includegraphics[width=0.51\textwidth]{plots/GPU_VecAXPY_flops.png}
    \includegraphics[width=0.51\textwidth]{plots/CPU_VecAXPY_flops.png}
    \caption{VecAXPY flop rate on GPUs, with one MPI rank per GPU, (left) and CPUs (right).}
    \label{fig:VecAXPY_flop_rate}
\end{figure}

Figures \ref{fig:virtualization}, \ref{fig:virtualization_latency}, and \ref{fig:veccudacopyto} explore the performance effects of GPU virtualization. Performance of the GPUs is similar even when divided into up to eight virtual GPUs for smaller  vectors. For larger vectors the performance is up to about 20\% worse. Note that in Figure \ref{fig:virtualization} the vectors on six GPUs are 1/6 the size they are on one GPU, this means the effective throughput numbers each are utilizing are lower than may have been expected, since the six GPUs no longer have long enough vectors for full performance. 

\begin{figure}[H]
    \centering
    \hspace{-.25in}
    \includegraphics[width=0.51\textwidth]{plots/VecDot_virtualization_10^8.png}
    \includegraphics[width=0.51\textwidth]{plots/VecAXPY_virtualization_10^8.png}
    \caption{Performance of GPU virtualization for VecDot operations (left) and VecAXPY (right) for vectors of length $10^8$.}
    \label{fig:virtualization}
\end{figure}

\begin{figure}
\begin{minipage}[b]{0.51\linewidth}\centering
\hspace{-.3in}
\includegraphics[scale=0.5]{plots/VecAXPY_virtualization_1_GPU.png}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}[b]{0.45\linewidth}
\centering
\input{plots/VecAXPY_virtualization_latency_table.txt}
\vspace{.3in}
\end{minipage}
\caption{VecAXPY virtualization performance for small vectors ($10^5 - 10^7$) on 1 GPU with latency ($10^{-6}$ seconds) and bandwidth (8 Mbytes/second) as defined in Section \ref{sec:discussion}.}
\label{fig:virtualization_latency}
\end{figure}


\begin{figure}[H]
    \centering
    \hspace{-.25in}
    \includegraphics[width=0.51\textwidth]{plots/VecCUDACopyTo_pinned.png}
    \caption{Effect of vector size on CPU to GPU copy throughput with 6 GPUs (multiple MPI ranks per GPU) and 1 GPU with 1 MPI rank. Pinned memory is always used. }
    \label{fig:veccudacopyto}
\end{figure}

Figure \ref{fig:veccudacopyto} shows the performance of CPU to GPU copies. For large vectors, memory transfers nearly reach the hardware peak.


Figures \ref{fig:flop_rate_vec_size} and \ref{fig:flop_rate_vec_size_normed}  show the performance of the vector operations as a function of vector size for different number of GPUs and CPU cores. Since the GPUs are independent entities the performance scales essentially perfectly for more GPUs. The CPU cores are not independent, as they share a common memory, and hence performance improvement decreases as more cores are utilized.

\begin{figure}[H]
    \centering
    \hspace{-.25in}
    \includegraphics[width=0.51\textwidth]{plots/CPU_GPU_VecDot_flops.png}
    \includegraphics[width=0.51\textwidth]{plots/CPU_GPU_VecAXPY_flops.png}
    \caption{Effect of vector size on flop rate for VecDot (left) and VecAXPY (right) on the CPU and GPUs with one MPI rank per GPU.}
    \label{fig:flop_rate_vec_size}
\end{figure}

\begin{figure}[H]
    \centering
    \hspace{-.25in}
    \includegraphics[width=0.51\textwidth]{plots/CPU_GPU_VecDot_flops_norm.png}
    \includegraphics[width=0.51\textwidth]{plots/CPU_GPU_VecAXPY_flops_norm.png}
    \caption{Effect of vector size on flop rate for VecDot (left) and VecAXPY (right), on the CPU and GPUs with one MPI rank per GPU, scaled by the number of CPU cores or GPUs.}
    \label{fig:flop_rate_vec_size_normed}
\end{figure}

\section{Discussion}\label{sec:discussion}

The performance of the basic vector operations as a function of vector size $n$  is complicated on modern CPU and GPU systems. The traditional linear model is given by 
\[
    t = latency + \frac{n}{bandwidth} = l + \frac{n}{b}. 
\]
In Table \ref{tab:vec_gpu_latency}  we show the bandwidth (computed via least squares) and latency (computed via least squares or obtained directly from the data for small vector sizes) pairs for each operation for 6 GPUs with 1 MPI rank per GPU. We found that operations on the GPUs follow this linear latency/bandwidth model for large vectors, the behavior for small vectors is depicted in Figure \ref{fig:small_vec_time}, left. For small vectors performance is completely determined by latency. Counter intuitively, working with slightly larger vectors takes (slightly) less time than shorter vectors. This odd behavior is because the GPU has thousands of computational units and until they are all occupied no additional time is needed for additional computations that now occupy some of the previously unoccupied units. This behavior is not captured in the linear model. The CPU has similar ``flat" behavior but only for even smaller vector sizes, Figure \ref{fig:small_vec_time}, right. Table \ref{tab:vec_cpu_latency} contains the latencies and bandwidths for the CPU. Operations on the CPU do not closely match a linear model for any range of vector sizes, thus the latencies are not computed via least squares. 

Table \ref{tab:cpu_gpu_latency} contains the latency and bandwidth values for copies from the CPU to the GPU, we found that for non-trivial size vectors the performance is well-modeled by the linear model.


\begin{table}[H]
    \centering
    \input{plots/GPU_latency_table_6GPU.txt}
    \caption{Latency ($10^{-6}$ seconds) and bandwidth (8 Mbytes/second) for vector operations on 6 GPUs.}
    \label{tab:vec_gpu_latency}
\end{table}

\begin{table}[H]
    \centering
    \input{plots/CPU_latency_table_42CPU.txt}
    \caption{Latency ($10^{-6}$ seconds) and bandwidth (8 Mbytes/second) for vector operations on 42 CPU cores.}
    \label{tab:vec_cpu_latency}
\end{table}

\begin{figure}[H]
    \centering
    \hspace{-.25in}
    \includegraphics[width=0.51\textwidth]{plots/GPU_VecDot_VecAXPY_time.png}
    \includegraphics[width=0.51\textwidth]{plots/CPU_VecDot_VecAXPY_time_42CPU_.png}
    \caption{GPU and CPU execution time on small vectors.}
    \label{fig:small_vec_time}
\end{figure}

\begin{table}[H]
    \centering
    \input{plots/CPU_GPU_latency_table_6GPU.txt}
    \caption{Latency ($10^{-6}$ seconds) and bandwidth (8 Mbytes/second) for copies from the CPU to 6 GPUs.}
    \label{tab:cpu_gpu_latency}
\end{table}

Even though the linear model does not capture the full behavior of the system it is still useful in understanding and comparing systems behavior.
Given the linear model above the throughput can be modeled as
\[
    T(n) = \frac{n}{l + \frac{n}{b}} = \frac{n \times b}{l \times b + n}. 
\]
The characteristic shape of this curve is reflected in the GPU values in Figure \ref{fig:vec_CPU_vs_GPU} as well as Figures 8, 9, and 10. The ``hump" in the CPU performance in Figure \ref{fig:vec_CPU_vs_GPU} reflects the more complicated memory hierarchy of the CPU; note that since we flush the caches before the vector operations and there is no data reuse the simple vector operations the hump is not simply due to cache resident affects.

The throughput, as a function of time, as depicted in Figure 3, can be modeled with\footnote{For $t > l$.}
\[
    T(t) = \frac{b(t - l)}{t} = b(1 - \frac{l}{t}). 
\]
The bandwidth provides the ``height'' of the curve and the latency is the ``start'' of the curve. One can understand the shape of the curve from a simple asymptotic analysis. For $ t $ at the latency
\[
  \frac{d ~ T }{d t}|_{t=l} = \frac{b}{l},
\]
hence the very steep initial slope. For extremely large $t$, the throughput approaches the bandwidth. In the discussion below we will use the machine characteristics of Summit with the VecAXPY operation to provide examples for the models; $l_G = 89 \times 10^{-6}$, $b_G = 5.176 \times 10^{12}$, $ l_C = 6 \times 10^{-6}$, and $ b_C = 256 \times 10^9$. Since latencies for large vector sizes are not available on the CPU we will use the value for the small vectors.

In the following we assume perfect scalability, that is adding more nodes does not increase or change the timings of the portions of the calculations. Of course this is certainly not true, but it can still produce useful information on the potential of the computations. From the model one can trivially derive formulas for the time and vector sizes needed to achieve any percentage, $ \frac{\beta}{\beta + 1} $, of the peak performance using
\[
    T(t_{\beta}) = b (1 - \frac{l}{t_{\beta}}) = \frac{\beta}{\beta + 1} \times b. 
\]
Thus $t_{\beta} = (\beta + 1) l$. Similarly $ n_{\beta} = \beta  \times l  \times  b$. So, for example, to obtain 90\% of peak on the GPUs one needs a vector length of $ 9  l_G  \times  b_G$, on Summit this is $ 4.146 \times 10^9. $ To achieve 99\% of peak one needs a vector length of $ 99 l_G  \times  b_G$. The combination of latency and bandwidth provides:
\begin{itemize}
    \item An easy way to determine, for a given size problem, the \textbf{percentage of peak you will achieve}. 
    \[
       \beta  = \frac{n}{l  \times  b}.
    \]
    \item A way to compare two systems. 
    \begin{itemize}
    
    \item  To achieve the \textbf{same  $ \beta$ } on both systems, the ratio of the time is the ratio of the latencies while the ratio of the needed problem sizes is the ratio of each system's $ l  \times  b$. For example, on Summit since $l_G/l_C = 15$ the GPUs will take 15 times longer to reach the goal. Similarly, since $b_G/b_C = 20$ the problem size on the GPU must be 300 times larger on the GPU.
    
    \item To solve the \textbf{same size problem}, $n$ on both systems the ratio of the times is
    \[
       \frac{t_C}{t_G} = \frac{l_C + \frac{n}{b_C}}{l_G + \frac{n}{b_G}}
    \]
    On Summit, if we select the size that is needed to achieve 90\% of peak on the GPUs the time required on the CPUs will be 18.2 times as long.

    \item To achieve the \textbf{same run-time} on both systems by increasing the number of the CPUs used let $ n_C $ be the vector size on a single CPU system. Then
    \[
       l_C + \frac{n_C}{b_C} = l_G + \frac{n}{b_G}
    \]
    resulting in
    \[
    n_C = b_C(\frac{n}{b_G} + l_G  - l_C ).
    \]
    Again, if $ n = 4.146 \times 10^9 $, that is achieving 90\% of peak on the GPU, then $ n_C = 226 \times 10^6$ and the number of CPU units needed is 19. Note that if you ignore latencies then both the ratio of the times and number of nodes needed is the ratio of the bandwidths.
    
    \item To achieve the \textbf{runtime dictated by the latency of the CPU system}, rather than the GPU system (assuming 90\% utilization of both) then the number of CPU nodes needed is 
    \[
        \frac{n}{n_C} = \frac{l_G  \times  b_G}{l_C  \times  b_C}. 
    \]
    For Summit this would require 300 nodes, but note the runtime would be seven times faster.
    \end{itemize}    

    \item A way to \textbf{select the number of nodes to use for a given size problem to come close to minimizing the runtime}. First note that the latency provides an absolute lower bound on the compute time, regardless of the problem size and the number of nodes available. Assume you are willing to accept a lower efficiency $\beta_s$ in return for a faster compute time with more units, then $ t_s = \frac{\beta_s + 1}{\beta + 1} t$. The new number of processors is given by $ \frac{\beta}{\beta_s}$. Thus if one desires to cut the time by  $ \frac{1}{k}$, they must use a $ \beta_s = \frac{\beta +1}{k} - 1 $ and the number of processors needed is $ \frac{k  \times  \beta}{\beta + 1 - k}.  $ If $\beta $ is 9 (90\% efficiency) and $ k $ is 2 (half the run time) then the number of nodes needed increases by a factor $ 2.2 $. If instead one desires to run 4 times faster then the number of processors increases by $ 6.$ To speedup by $8$ would require $36$ times as many nodes. Note that the bandwidth of neither system plans any role in the analysis.
\end{itemize}


Table \ref{tab:summary} provides a summary of the Summit system and PETSc's vector performance on one node. 
This is followed by notes and observations on the data.

\begin{table}[H]
    \centering
    \input{plots/summarytable.txt}
    \caption{Summary of PETSc vector performance on Summit. Latency is in $ 10^{-6}$ seconds, bandwidth in 8,000 Mbytes/second.}
    \label{tab:summary}
\end{table}

\begin{itemize}
    \item When performing scaling studies on a node (that is increasing CPU processor or GPU counts) it is crucial to use the correct resource sets otherwise the results can be misleading.
    
    \item GPU virtualization has a small negative impact (up to 20\% on vectors of length $10^8$) on the performance of the GPU vector computations, Figures \ref{fig:virtualization} and \ref{fig:virtualization_latency}. For codes that contain a significant portion of CPU computations it makes sense to use many or all of the CPU cores as MPI ranks and have them share the virtualized GPUs; for pure GPU computation there is no benefit to virtualization and possibly a performance lose.
    
    \item Using pinned memory is crucial to achieve high throughput in copying between the CPU and GPU. We found a 4.5 times speedup of pinned over non-pinned memory for large vectors and smaller speedups for small vectors. 

    \item Timing results can be collected on the GPU, on the CPU or some combination of the two. It is important to understand the goal of the data collection in deciding which timings to collect.
    
    \item We do not measure the throughput directly between GPUs since PETSc currently has no mechanism to utilize this hardware. 
    
    \item The performance of the vector operations on the GPU are affected by a combination of latencies. These include
    \begin{itemize}
    \item CPU to GPU launch latency\footnote{Determined by a simple program that measures the latency for a no-op kernel; the time for $tkernel<<<2000, 32>>>()$; with $ \_global\_\_ void tkernel()\{\}$ \cite{cuda-startup}}. Table 3.
    \item GPU kernel synchronization (wait) time with the CPU\footnote{Time waiting on the CPU for the result {\em cudaDeviceSynchronize()}.} Table 3. Note that this means, for example, only around 100,000 kernel launches are possible in one second.
    \item Main GPU memory access latency.
    \item Occupancy of all of the thousands of compute units.
    \item For routines that return values to the CPU, such as VecDot, the additional latency of that the data movement back to the CPU. Note that this means, for example, only around 50,000 results from inner products communicated from the GPU to the CPU are possible in one second. 
\end{itemize}

%We provide the details on where in the report each item in Table \ref{tab:summary} was collected.

%\begin{itemize}
%    \item The latency for data transfer from the CPU to the GPU comes from Table \ref{tab:cpu_gpu_latency}. See also Figure \ref{fig:veccudacopyto}, right.
    
%     \item The latency for VecDot and VecAXPY with small vectors comes from Figure \ref{fig:small_vec_time}, right, which are significantly lower than for the GPU, left. These numbers are also reflected in Tables \ref{tab:vec_gpu_latency} and \ref{tab:vec_cpu_latency}. 
    
%    \item For large vectors the bandwidth on the GPUs comes from Figure \ref{fig:vec_CPU_vs_GPU}, left,  (compare also the bandwidth numbers in Table 1 and 2, first row, last column). Vectors must be of at least size $10^7$ on a node (combined six GPUs) to begin to achieve the peak bandwidth for the GPUs and  $10^5$ for the CPU (combined 42 CPUs). About 97\% = 31/32 of the node's memory bandwidth is hosted on the GPU.  

%    \end{itemize}
 
\end{itemize}

The performance of the PETSc VecAXPY operation on a Summit node can be summarized as: much higher bandwidth on the GPUs (20 times higher), significantly lower latencies on the CPU (15 times lower). Much larger vectors are required on the GPU to achieve high performance (over 300 times larger, the product of the ratios of the bandwidth and latency).

\section*{Acknowledgments}

\noindent We thank Junchao Zhang for several crucial technical corrections. We thank Jed Brown for introducing us to the work-time spectrum plot. We thank Todd Munson and Karl Rupp for their useful comments to improve this report. \\

\noindent The authors were supported by the U.S. Department of Energy,
Office of Science, Advanced Scientific Computing Research under Contract DE-AC02-06CH11357. \\ 

\noindent This research was supported by the Exascale Computing Project (17-SC-20-SC), a collaborative effort of the U.S. Department of Energy Office of Science and the National Nuclear Security Administration. \\

\noindent This research used resources of the Oak Ridge Leadership Computing Facility at the Oak Ridge National Laboratory, which is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC05-00OR22725. \\



\bibliographystyle{plain}

\bibliography{bibli}

\includepdf[pages={5}]{ANL_tech_report_title_page.pdf}

\end{document}
